using UnityEngine;
using System.Collections;
using System;

public class GameSettings : MonoBehaviour {
	public const string PLAYER_SPAWN_POINT = "PlayerSpawnPoint";
	
	void Awake () {
		DontDestroyOnLoad(this);
	}
	
	
	public void SaveCharacterData(){
		GameObject pc = GameObject.Find("pc");
		
		PlayerCharacter pcClass=pc.GetComponent<PlayerCharacter>();
		
		
		///
		//PlayerPrefs.DeleteAll();
		///
		
		
		PlayerPrefs.SetString("PlayerName",pcClass.Name);
		
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(AttributeName)).Length;cnt++){
			PlayerPrefs.SetInt(((AttributeName)cnt).ToString() + " - Base Value",pcClass.GetPrimaryAttribute(cnt).BaseValue);
			PlayerPrefs.SetInt(((AttributeName)cnt).ToString() + " - Exp To Level",pcClass.GetPrimaryAttribute(cnt).ExpToLevelValue);
		}
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(VitalName)).Length;cnt++){
			PlayerPrefs.SetInt(((VitalName)cnt).ToString() + " - Base Value",pcClass.GetVital(cnt).BaseValue);
			PlayerPrefs.SetInt(((VitalName)cnt).ToString() + " - Exp To Level",pcClass.GetVital(cnt).ExpToLevelValue);
			PlayerPrefs.SetInt(((VitalName)cnt).ToString() + " - Current Value",pcClass.GetVital(cnt).CurValue);
			
			//PlayerPrefs.SetString(((VitalName)cnt).ToString() + " - Mods",pcClass.GetVital(cnt).GetModifingAttributsString());
		}		
		
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(SkillName)).Length;cnt++){
			PlayerPrefs.SetInt(((SkillName)cnt).ToString() + " - Base Value",pcClass.GetSkill(cnt).BaseValue);
			PlayerPrefs.SetInt(((SkillName)cnt).ToString() + " - Exp To Level",pcClass.GetSkill(cnt).ExpToLevelValue);
			
			 //PlayerPrefs.SetString(((SkillName)cnt).ToString() + " - Mods",pcClass.GetSkill(cnt).GetModifingAttributsString());			
		}		
		
		
	}

	public void LoadCharacterData(){
		GameObject pc = GameObject.Find("pc");		
		PlayerCharacter pcClass=pc.GetComponent<PlayerCharacter>();		
		pcClass.Name = PlayerPrefs.GetString("PlayerName","Name Me");
		
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(AttributeName)).Length;cnt++){
			pcClass.GetPrimaryAttribute(cnt).BaseValue = PlayerPrefs.GetInt(((AttributeName)cnt).ToString() + " - Base Value",0);
			pcClass.GetPrimaryAttribute(cnt).ExpToLevelValue = PlayerPrefs.GetInt(((AttributeName)cnt).ToString() + " - Exp To Level",Attribute.STARTING_EXP_COST);
		}
		
		
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(VitalName)).Length;cnt++){
			pcClass.GetVital(cnt).BaseValue = PlayerPrefs.GetInt(((VitalName)cnt).ToString() + " - Base Value",0);
			pcClass.GetVital(cnt).ExpToLevelValue = PlayerPrefs.GetInt(((VitalName)cnt).ToString() + " - Exp To Level",0);
						
			pcClass.GetVital(cnt).Update();
			pcClass.GetVital(cnt).CurValue = PlayerPrefs.GetInt(((VitalName)cnt).ToString() + " - Current Value",1);
		}		
		
		for(int cnt=0;cnt<Enum.GetValues(typeof(SkillName)).Length;cnt++){
			pcClass.GetSkill(cnt).BaseValue = PlayerPrefs.GetInt(((SkillName)cnt).ToString() + " - Base Value",0);
			pcClass.GetSkill(cnt).ExpToLevelValue = PlayerPrefs.GetInt(((SkillName)cnt).ToString() + " - Exp To Level",0);
		}		
					

		for(int cnt=0;cnt<Enum.GetValues(typeof(SkillName)).Length;cnt++){
			Debug.Log (((SkillName)cnt).ToString() + " : "+ pcClass.GetSkill(cnt).BaseValue + " : " + pcClass.GetSkill(cnt).ExpToLevelValue);
		}

		
	}
}
