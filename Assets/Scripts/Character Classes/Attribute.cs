public class Attribute : BaseStat {
	new public const int STARTING_EXP_COST = 50;
	
	public Attribute(){		
		ExpToLevelValue = 50;
		LevelModifierValue = 1.05f;
		
	}
}


public enum AttributeName{
	Might,
	Constitution,
	Nimbleness,
	Speed,
	Concentration,
	Willpower,
	Charisma
}
	