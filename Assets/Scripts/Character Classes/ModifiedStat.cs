using System.Collections.Generic;

public class ModifiedStat : BaseStat {
	private List<ModifyingAttribute> _mods;		// List of attributes that modify the stat
	private int _modValue;						// The amount added to the baseValue from the modifiers
	
	public ModifiedStat(){
		_mods = new List<ModifyingAttribute>();
		_modValue = 0;
	}	
	
	public void AddModifier(ModifyingAttribute mod){
		_mods.Add(mod);
	}
	
	private void CalculateModValue(){
		_modValue = 0;
		
		if (_mods.Count>0){
			foreach(ModifyingAttribute att in _mods){
				_modValue += (int)(att.attribute.AdjustedBaseValue * att.ratio);
			}
		}
	}
	
	public new int AdjustedBaseValue {
		get{return BaseValue + BuffValue + _modValue;}
	}
	
	public void Update() {
		CalculateModValue();
	}
	
	public string GetModifingAttributsString(){
		string tempString = "";
		
		for (int cnt=0;cnt<_mods.Count;cnt++){
			tempString += _mods[cnt].attribute.Name;
			tempString += "_";
			tempString += _mods[cnt].ratio;	
			
			if(cnt < _mods.Count -1){
				tempString += "|";
			}
		}
		return tempString;
	}
}


public struct ModifyingAttribute{
	public Attribute attribute;
	public float ratio;
	
	/// <summary>
	/// Initializes a new instance of the <see cref="ModifyingAttribute"/> struct.
	/// </summary>
	/// <param name='att'>
	/// Att.
	/// </param>
	/// <param name='rat'>
	/// Rat.
	/// </param>
	public ModifyingAttribute(Attribute att, float rat){
		attribute = att;
		ratio = rat;
	}
}