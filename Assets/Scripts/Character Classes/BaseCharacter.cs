using UnityEngine;
using System.Collections;
using System;					// To access Enum class

public class BaseCharacter : MonoBehaviour {
	private string _name;
	private int _level;
	private uint _freeExp;
		
	private Attribute[] _primaryAttribute;
	private Vital[] _vital;
	private Skill[] _skill;
	
	public void Awake() {
		_name = string.Empty;
		_level = 0;
		_freeExp = 0;
		
		_primaryAttribute = new Attribute[Enum.GetValues(typeof(AttributeName)).Length];
		_vital = new Vital[Enum.GetValues(typeof(VitalName)).Length];
		_skill = new Skill[Enum.GetValues(typeof(SkillName)).Length];
		
		SetupPrimaryAttributes();
		SetupVitals();
		SetupSkills();
	}
	

#region Getters and setters
	public string Name {
		get{return _name;}
		set{_name = value;}
	}
	
	public int Level{
		get{return _level;}
		set{_level = value;}
	}
	
	public uint FreeExp {
		get{return _freeExp;}
		set{_freeExp = value;}
	}
	
#endregion
	
	public void AddExp(uint exp) {
		FreeExp += exp;
		CalculateLevel();
	}
	
	
	//TODO take avg of all players skills and assign that as player level
	public void CalculateLevel(){
		
	}
	
	private void SetupPrimaryAttributes() {
		for(int cnt=0;cnt < _primaryAttribute.Length; cnt++){
			_primaryAttribute[cnt] = new Attribute();
			_primaryAttribute[cnt].Name = ((AttributeName)cnt).ToString();
		}
	}
	
	private void  SetupVitals(){
		for(int cnt=0;cnt < _vital.Length; cnt++){
			_vital[cnt] = new Vital();
		}
		SetupVitalModifiers();
	}
	
	private void  SetupSkills(){
		for(int cnt=0;cnt < _skill.Length; cnt++){
			_skill[cnt] = new Skill();
		}
		SetupSkillModifiers();
	}
	
	
	public Attribute GetPrimaryAttribute(int index) {
		return _primaryAttribute[index];
	}
	
	public Vital GetVital(int index) {
		return _vital[index];
	}
	
	public Skill GetSkill(int index) {
		return _skill[index];
	}	
	
	private void SetupVitalModifiers(){

		//Health
		ModifyingAttribute healthModifier = new ModifyingAttribute();
		healthModifier.attribute = GetPrimaryAttribute((int)AttributeName.Constitution);
		healthModifier.ratio = 0.5f;	
		
		GetVital((int)VitalName.Health).AddModifier(healthModifier);
		
		
		//Energy
		
		ModifyingAttribute energyModifier = new ModifyingAttribute();
		energyModifier.attribute = GetPrimaryAttribute((int)AttributeName.Constitution);
		energyModifier.ratio = 1.0f;	
		
		GetVital((int)VitalName.Energy).AddModifier(energyModifier);
		
		
		//Mana
		
		ModifyingAttribute manaModifier = new ModifyingAttribute();
		manaModifier.attribute = GetPrimaryAttribute((int)AttributeName.Willpower);
		manaModifier.ratio = 1.0f;	
		
		GetVital((int)VitalName.Mana).AddModifier(manaModifier);
		
	}
	
	private void SetupSkillModifiers(){
		ModifyingAttribute meleeMod1 = new ModifyingAttribute();
		ModifyingAttribute meleeMod2 = new ModifyingAttribute();
		
		meleeMod1.attribute = GetPrimaryAttribute((int)AttributeName.Might);	
		meleeMod1.ratio = 0.33f;
		
		meleeMod2.attribute = GetPrimaryAttribute((int)AttributeName.Nimbleness);	
		meleeMod2.ratio = 0.33f;
				
		GetSkill((int)SkillName.Melee_Offense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Melee_Offense).AddModifier(meleeMod2);
		
		
				
		meleeMod1.attribute = GetPrimaryAttribute((int)AttributeName.Speed);	
		meleeMod2.attribute = GetPrimaryAttribute((int)AttributeName.Constitution);					
		GetSkill((int)SkillName.Melee_Defense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Melee_Defense).AddModifier(meleeMod2);

		meleeMod1.attribute = GetPrimaryAttribute((int)AttributeName.Concentration);	
		meleeMod2.attribute = GetPrimaryAttribute((int)AttributeName.Willpower);					
		GetSkill((int)SkillName.Magic_Offense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Magic_Offense).AddModifier(meleeMod2);				
		GetSkill((int)SkillName.Magic_Defense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Magic_Defense).AddModifier(meleeMod2);
		
		meleeMod1.attribute = GetPrimaryAttribute((int)AttributeName.Concentration);	
		meleeMod2.attribute = GetPrimaryAttribute((int)AttributeName.Speed);					
		GetSkill((int)SkillName.Ranged_Offense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Ranged_Offense).AddModifier(meleeMod2);
		
		meleeMod1.attribute = GetPrimaryAttribute((int)AttributeName.Speed);	
		meleeMod2.attribute = GetPrimaryAttribute((int)AttributeName.Nimbleness);					
		GetSkill((int)SkillName.Ranged_Defense).AddModifier(meleeMod1);
		GetSkill((int)SkillName.Ranged_Defense).AddModifier(meleeMod2);
	}
	
	public void StatUpdate(){
		for(int cnt = 0;cnt < _vital.Length;cnt ++){
			_vital[cnt].Update();
		}
		
		for(int cnt = 0;cnt < _skill.Length;cnt ++){
			_skill[cnt].Update();
		}		
	}
}
