// <summary>
// 
// This is the base class for stats in game
//
// </summary>
public class BaseStat  {
	public const int STARTING_EXP_COST = 100;
	
	private int _baseValue;			// Base value
	private int _buffValue;			// Amount of buff to the stat
	private int _expToLevel;		// The total amount of exp needed to raise this skill
	private float _levelModifier;	// The modifier applied to the exp  needed  to raiae the skill
	private string _name;
	
	public BaseStat() {
		_baseValue = 0;
		_buffValue = 0;
		_levelModifier = 1.1f;
		_expToLevel = 100;
		_name = "";
	}
	
	
#region Basic setters and getters
	// Setters and getters
	
	/// <summary>
	/// Gets or sets the base value.
	/// </summary>
	/// <value>
	/// The base value.
	/// </value>
	public int BaseValue {
		get {return _baseValue;}
		
		set {_baseValue = value;}
	}
	
	public int BuffValue {
		get {return _buffValue;}
		
		set {_buffValue = value;}
	}
	
	public int ExpToLevelValue {
		get {return _expToLevel;}
		
		set {_expToLevel = value;}
	}
	public float LevelModifierValue {
		get {return _levelModifier;}
		
		set {_levelModifier = value;}
	}
	
	public string Name{
		get{ return _name; }
		set{ _name = value; }
	} 
#endregion
	
	private int CalculateExpToLevel(){
		return (int)(_expToLevel * _levelModifier);
	}
	
	public void LevelUp() {
		_expToLevel = CalculateExpToLevel();
		_baseValue++;
	}
	
	public int AdjustedBaseValue {
		get{return _baseValue + _buffValue;}		
	}
}
